export default async function asyncForEach(iterable = [], func, errFunc) {
	for await (const item of iterable) {
		try {
			func(item);
		} catch (e) {
			if (errFunc) {
				errFunc(e);
				continue;
			}
			throw(e);
		}
	}
}